export const GroupBy = {
  None: 'none',
  Label: 'label',
};

export const iterationStates = {
  closed: 'closed',
  upcoming: 'upcoming',
  expired: 'expired',
  all: 'all',
};

export const iterationSortDesc = 'CADENCE_AND_DUE_DATE_DESC';
